package com.example;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MovableFloatingActionButton extends FloatingActionButton implements View.OnTouchListener {

    private final static float CLICK_DRAG_TOLERANCE = 10; // Often, there will be a slight, unintentional, drag when the user taps the FAB, so we need to account for this.

    private float downRawX, downRawY;
    private float dX, dY;
    private View viewParent;
    private int parentWidth;
    private int parentHeight;
    private int viewWidth;
    private int viewHeight;

    public MovableFloatingActionButton(Context context) {
        super(context);
        init();
    }

    public MovableFloatingActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MovableFloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent){
        viewWidth = view.getWidth();
        viewHeight = view.getHeight();

        viewParent = (View)view.getParent();
        parentWidth = viewParent.getWidth();
        parentHeight = viewParent.getHeight();

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();

        int action = motionEvent.getAction();
        if (action == MotionEvent.ACTION_DOWN) {

            downRawX = motionEvent.getRawX();
            downRawY = motionEvent.getRawY();
            dX = view.getX() - downRawX;
            dY = view.getY() - downRawY;

            return true; // Consumed

        }
        else if (action == MotionEvent.ACTION_MOVE) {
            float newX = motionEvent.getRawX() + dX;
            newX = Math.max(layoutParams.leftMargin, newX); // Don't allow the FAB past the left hand side of the parent
            newX = Math.min(parentWidth - viewWidth - layoutParams.rightMargin, newX); // Don't allow the FAB past the right hand side of the parent

            float newY = motionEvent.getRawY() + dY;
            newY = Math.max(layoutParams.topMargin, newY); // Don't allow the FAB past the top of the parent
            newY = Math.min(parentHeight - viewHeight - layoutParams.bottomMargin, newY); // Don't allow the FAB past the bottom of the parent

            view.animate()
                    .x(newX)
                    .y(newY)
                    .setDuration(0)
                    .start();

            return true; // Consumed

        }
        else if (action == MotionEvent.ACTION_UP) {

            float upRawX = motionEvent.getRawX();
            float upRawY = motionEvent.getRawY();

            float upDX = upRawX - downRawX; //equals to view.getX
            float upDY = upRawY - downRawY; //equals to view.getY

            int batasLeft = layoutParams.leftMargin; //0
            int batasRight = parentWidth - viewWidth - layoutParams.rightMargin; //926
            int batasTop = layoutParams.topMargin; //0
            int batasBottom = parentHeight - viewHeight - layoutParams.bottomMargin; //1912

            float koordinatX = view.getX();
            float koordinatY = view.getY();

            float rangeKiri = koordinatX;
            float rangeKanan = batasRight - koordinatX;
            float rangeAtas = koordinatY;
            float rangeBawah = batasBottom - koordinatY;

            float minimumKiriKanan = Math.min(rangeKiri, rangeKanan);
            float minimumAtasBawah = Math.min(rangeAtas, rangeBawah);
            float globalMinimum = Math.min(minimumKiriKanan, minimumAtasBawah);

            if(globalMinimum == minimumKiriKanan){
                if(minimumKiriKanan == rangeKiri) view.setX(0);
                else if(minimumKiriKanan == rangeKanan) view.setX(batasRight);
            }
            else if(globalMinimum == minimumAtasBawah){
                if(minimumAtasBawah == rangeAtas) view.setY(0);
                else if(minimumAtasBawah == rangeBawah) view.setY(batasBottom);
            }

            Log.d("<TEST>", "X: " + koordinatX);
            Log.d("<TEST>", "Y: " + koordinatY);
            Log.d("<TEST>", "Kiri: " + rangeKiri);
            Log.d("<TEST>", "Kanan: " + rangeKanan);
            Log.d("<TEST>", "Atas: " + rangeAtas);
            Log.d("<TEST>", "Bawah: " + rangeBawah);
            Log.d("<TEST>", "FINISHED!");

            if (Math.abs(upDX) < CLICK_DRAG_TOLERANCE && Math.abs(upDY) < CLICK_DRAG_TOLERANCE) { // A click
                return performClick();
            }
            else { // A drag
                return true; // Consumed
            }

        }
        else {
            return super.onTouchEvent(motionEvent);
        }

    }

}
